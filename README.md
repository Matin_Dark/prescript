# Run script
Follow [this](https://docs.fivem.net/server-manual/setting-up-a-server/) guide to install your root directory then 
just clone the project into the folder

# Setup Database
I include two types of database which they are available in **/database/database-withdata** which have lastest players data from MadCity
And other one is in **/database/database-withoutdata (only structure)** which does not have players data and it's raw structure
After you import one of those database files you should go to
- server.cfg
- resources/ghmattimysql/config.json

and edit the database information such as username, password and database route and enjoy the game mode

**I am not the creator of the ESX I just edited them heavily to create the best experience for a roleplay server and I am not responsible for any issue or bug this game mode have, use it with your own risk**