local ESX = nil
local timePlay = {}
local NewPlayers = {}

-- ESX
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('esx:playerLoaded', function(source)

    local _source = source
    local identifier = GetPlayerIdentifier(_source)
    timePlay[identifier] = {source = _source, joinTime = os.time(), timePlay = 0}
    MySQL.Async.fetchAll("SELECT timePlay FROM users WHERE identifier = @identifier", { ["@identifier"] = identifier }, function(result)

        if result then

            local timePlayP = result[1].timePlay
            timePlay[identifier].timePlay = timePlayP
            
            if timePlayP < 43200 then

                NewPlayers[identifier] = {source = _source}
                TriggerClientEvent('timeplay:set_tags', -1, NewPlayers)
        
            else

                TriggerClientEvent('timeplay:set_tags', -1, NewPlayers)

            end

        else
            print('Moshkeli dar gereftan timePlay ' .. GetPlayerName(_source) .. ' pish amad lotfan peygiri konid!')
        end

    end)

end)


RegisterCommand('debugt', function(source)
    print(ESX.dump(timePlay))
    print('----------------------')
    print('      New Players     ')
    print('----------------------')
    print(ESX.dump(NewPlayers))
end, false)

AddEventHandler('playerDropped', function()
	
	local _source = source
        if _source ~= nil then
            local identifier = GetPlayerIdentifier(_source)

            if timePlay[identifier] ~= nil then

                local leaveTime = os.time()
                local saveTime = leaveTime - timePlay[identifier].joinTime

                MySQL.Async.execute('UPDATE users SET timePlay = timePlay + @timePlay WHERE identifier=@identifier', 
                {
                    ['@identifier'] = identifier,
                    ['@timePlay'] = saveTime
                    
                }, function(rowsChanged)

                    if rowsChanged == 0 then
                        print('Moshkeli dar save kardan timePlay ' .. GetPlayerName(_source) .. ' pish amad lotfan peygiri konid!')
                    end
                    timePlay[identifier] = nil
                    NewPlayers[identifier] = nil
                    TriggerClientEvent('timeplay:set_tags', -1, NewPlayers)

                end)

            end

        end

end)