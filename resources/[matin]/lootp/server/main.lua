ESX = nil
local Users = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('esx_thief:getValue', function(source, cb, targetSID)
	if Users[targetSID] then
		cb(Users[targetSID])
	else
		cb({value = false, time = 0})
	end
end)

ESX.RegisterServerCallback('esx_thief:getOtherPlayerData', function(source, cb, target)
	local xPlayer = ESX.GetPlayerFromId(target)

	local data = {
		name 		= GetPlayerName(target),
		job 		= xPlayer.job.name,
		inventory 	= xPlayer.inventory,
		accounts 	= xPlayer.accounts,
		money 		= xPlayer.money,
		weapons		= xPlayer.loadout
	}

	cb(data)
end)

RegisterServerEvent('esx_thief:stealPlayerItem')
AddEventHandler('esx_thief:stealPlayerItem', function(target, itemType, itemName, amount)
	local _source 		= source
	local sourceXPlayer = ESX.GetPlayerFromId(_source)
	local targetXPlayer = ESX.GetPlayerFromId(target)
	local oocname 		=  GetPlayerName(source)
	local targetName 	=  GetPlayerName(target)

	if not targetXPlayer then
		return
	end 

	if true then
		--and not (targetXPlayer.getGroup() == 'admin' or targetXPlayer.getGroup() == 'superadmin')
		if itemType == 'item_standard' then
			local label = sourceXPlayer.getInventoryItem(itemName).label
			local itemLimit = sourceXPlayer.getInventoryItem(itemName).limit
			local sourceItemCount = sourceXPlayer.getInventoryItem(itemName).count
			local targetItemCount = targetXPlayer.getInventoryItem(itemName).count
			if amount > 0 and targetItemCount >= amount then
				if itemLimit ~= -1 and (sourceItemCount + amount) > itemLimit then
					TriggerClientEvent('esx:showNotification', targetXPlayer.source, _U('ex_inv_lim_target'))
					TriggerClientEvent('esx:showNotification', sourceXPlayer.source, _U('ex_inv_lim_source'))
				else
					targetXPlayer.removeInventoryItem(itemName, amount)
					sourceXPlayer.addInventoryItem(itemName, amount)

					TriggerEvent('DiscordBot:ToDiscord', 'loot', oocname, 'Stole '..amount ..'X '.. itemName .. ' from ' .. targetName,'user', true, source, false)
					TriggerClientEvent('esx:showNotification', sourceXPlayer.source, _U('you_stole') .. ' ~g~x' .. amount .. ' ' .. label .. ' ~w~' .. _U('from_your_target') )
					TriggerClientEvent('esx:showNotification', targetXPlayer.source, _U('someone_stole') .. ' ~r~x'  .. amount .. ' ' .. label )

				end
			else
				TriggerClientEvent('esx:showNotification', _source, _U('invalid_quantity'))
			end

		elseif itemType == 'item_money' then

			if amount > 0 and targetXPlayer.get('money') >= amount then
				targetXPlayer.removeMoney(amount)
				sourceXPlayer.addMoney(amount)

				TriggerClientEvent('esx:showNotification', sourceXPlayer.source, _U('you_stole') .. ' ~g~$' .. amount .. ' ~w~' .. _U('from_your_target') )
				TriggerClientEvent('esx:showNotification', targetXPlayer.source, _U('someone_stole') .. ' ~r~$'  .. amount )
				TriggerEvent('DiscordBot:ToDiscord', 'loot', oocname, 'Stole '..amount ..'$ from ' .. targetName,'user', true, source, false)
			else
				TriggerClientEvent('esx:showNotification', _source, _U('imp_invalid_amount'))
			end

		elseif itemType == 'item_weapon' then
			local ammo = targetXPlayer.hasWeapon(itemName)

			if ammo then
				targetXPlayer.removeWeapon(itemName, ammo)
				sourceXPlayer.addWeapon(itemName, ammo)
		
				TriggerClientEvent('esx:showNotification', sourceXPlayer.source, _U('you_stole') .. ' ~g~x' .. ammo .. ' ' .. itemName .. ' ~w~' .. _U('from_your_target') )
				TriggerClientEvent('esx:showNotification', targetXPlayer.source, _U('someone_stole') .. ' ~r~x'  .. ammo .. ' ' .. itemName )
				TriggerEvent('DiscordBot:ToDiscord', 'loot', oocname, 'Stole '.. itemName .. ' with ' .. ammo .. ' bullets from ' .. targetName,'user', true, source, false)
			end
		end 
	else
		TriggerClientEvent('esx:showNotification', sourceXPlayer.source, 'Shoma Nemitonid in player ro robb konid. ~r~Qavanine robb ro bekhonid !')
	end
end)

 function dump(o)
	if type(o) == 'table' then
	   local s = '{ '
	   for k,v in pairs(o) do
		  if type(k) ~= 'number' then k = '"'..k..'"' end
		  s = s .. '['..k..'] = ' .. dump(v) .. ','
	   end
	   return s .. '} '
	else
	   return tostring(o)
	end
 end

RegisterServerEvent('esx_thief:update')
AddEventHandler('esx_thief:update', function(bool)
	local _source = source
	Users[_source] = {value = bool, time = os.time()}
end)

TriggerEvent('es:addCommand', 'getthelist', function(source, args, user)
	print(dump(Users))
end)

TriggerEvent('es:addCommand', 'wloadout', function(source, args, user)
	local targetXPlayer = ESX.GetPlayerFromId(args[1])
	print(dump(targetXPlayer.getLoadout()))
end)

RegisterServerEvent('esx_thief:getValue')
AddEventHandler('esx_thief:getValue', function(targetSID)
	local _source = source
	if Users[targetSID] then
		TriggerClientEvent('esx_thief:returnValue', _source, Users[targetSID])
	else
		TriggerClientEvent('esx_thief:returnValue', _source, Users[targetSID])
	end
end)