Config = {}

Config.Locale = 'fa'

Config.EnableCash       = true
Config.EnableBlackMoney = false
Config.EnableInventory  = true
Config.EnableWeapons    = true