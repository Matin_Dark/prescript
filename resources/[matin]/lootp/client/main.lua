local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function IsAbleToSteal(targetSID, err)
	ESX.TriggerServerCallback('esx_thief:getValue', function(result)
		local result = result
		if result.value then
			err(false)
		else
			err(_U('no_hands_up'))
		end
	end, targetSID)
end

function OpenStealMenu(target, target_id)
	ESX.UI.Menu.CloseAll()

	ESX.TriggerServerCallback('esx_thief:getOtherPlayerData', function(data)
		local elements = {}

		if Config.EnableCash then
			table.insert(elements, {
				label = (('[%s] $%s'):format(_U('cash'), ESX.Math.GroupDigits(data.money))),
				value = 'money',
				type = 'item_money',
				amount = data.money
			})
		end

		if Config.EnableBlackMoney then
			local blackMoney = 0

			for i=1, #data.accounts, 1 do
				if data.accounts[i].name == 'black_money' then
					blackMoney = data.accounts[i].money
					break
				end
			end

			table.insert(elements, {
				label = (('[%s] $%s'):format(_U('black_money'), ESX.Math.GroupDigits(blackMoney))),
				value = 'black_money',
				type = 'item_account',
				amount = blackMoney
			})
		end
		if Config.EnableInventory then
			table.insert(elements, {label = '--- ' .. _U('inventory') .. ' ---', value = nil})

			for i=1, #data.inventory, 1 do
				if data.inventory[i].count > 0 then
					if 
						( data.inventory[i].name == "medikit" or data.inventory[i].name == "bandage" )
						and
						(data.job == "ambulance" or data.job == "police")
					then
						noStateMentIsGoodOne = 0
					else
						table.insert(elements, {
							label = data.inventory[i].label .. ' x' .. data.inventory[i].count,
							value = data.inventory[i].name,
							type  = 'item_standard',
							amount = data.inventory[i].count,
						})
					end
				end
			end
		end

		if Config.EnableWeapons then
            table.insert(elements, {label = '=== ' .. _U('gun_label') .. ' ===', value = nil})

			for i=1, #data.weapons, 1 do
				if 
					(data.job == "police")
				then
					noStateMentIsGoodOne = 0
				else
					table.insert(elements, {
						label    = ESX.GetWeaponLabel(data.weapons[i].name) .. ' x' .. data.weapons[i].ammo,
						value    = data.weapons[i].name,
						type     = 'item_weapon',
						amount   = data.weapons[i].ammo
					})
				end
            end
        end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'steal_inventory', {
			title  = _U('target_inventory'),
			elements = elements,
			align = 'top-right'
		}, function(data, menu)

			if data.current.value ~= nil then

				local itemType = data.current.type
				local itemName = data.current.value
				local amount   = data.current.amount
				local elements = {}
				table.insert(elements, {label = _U('steal'), action = 'steal', itemType, itemName, amount})
				table.insert(elements, {label = _U('return'), action = 'return'})

				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'steal_inventory_item', {
					title = _U('action_choice'),
					align = 'top-right',
					elements = elements
				}, function(data2, menu2)
					if data2.current.action == 'steal' then

						if itemType == 'item_standard' then
							ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'steal_inventory_item_standard', {
								title = _U('amount')
							}, function(data3, menu3)
								local quantity = tonumber(data3.value)
								Wait(math.random(0,500))
								TriggerServerEvent('esx_thief:stealPlayerItem', GetPlayerServerId(target), itemType, itemName, quantity)
								OpenStealMenu(target)
							
								menu3.close()
								menu2.close()
							end, function(data3, menu3)
								menu3.close()
							end)
						else
							Wait(math.random(0,500))
							TriggerServerEvent('esx_thief:stealPlayerItem', GetPlayerServerId(target), itemType, itemName, amount)
							OpenStealMenu(target)
						end

					elseif data2.current.action == 'return' then
						ESX.UI.Menu.CloseAll()
						OpenStealMenu(target)
					end

				end, function(data2, menu2)
					menu2.close()
				end)
			end

		end, function(data, menu)
			menu.close()
		end)
	end, GetPlayerServerId(target))
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		
		local ped = PlayerPedId()

		if IsControlJustPressed(0, Keys['Z']) and IsPedArmed(ped, 7) and not IsEntityDead(ped) and IsPedOnFoot(ped) then
			local target, distance = ESX.Game.GetClosestPlayer()
			
			if target ~= -1 and distance ~= -1 and distance <= 1.5 then
				local target_id = GetPlayerServerId(target)
				local closestPlayerPed = GetPlayerPed(target)
				local anim = IsEntityPlayingAnim(closestPlayerPed, "amb@world_human_bum_slumped@male@laying_on_left_side@base" ,"base", 3 )
				if anim == 1 then
					OpenStealMenu(target, target_id)
				else
					IsAbleToSteal(target_id, function(err)
						if(not err)then
							OpenStealMenu(target, target_id)
						else
							ESX.ShowNotification(err)
						end
					end)
				end
			elseif distance < 20 and distance > 1.5 then
				ESX.ShowNotification(_U('too_far'))
			else
				ESX.ShowNotification(_U('no_players_nearby'))
			end
		end
	end
end)

-- -- No need to touch anything below.
-- Citizen.CreateThread(function()
    
--     local showHelp = true
--     local loaded = false
    
--     while true do
--         Citizen.Wait(0) -- prevent crashing
--         if IsNextWeatherType('XMAS') then -- check for xmas weather type
--             -- enable frozen water effect (water isn't actually ice, just looks like there's an ice layer on top of the water)
--             N_0xc54a08c85ae4d410(3.0)
--             -- preview: https://vespura.com/hi/i/2eb901ad4b1.gif
            
--             SetForceVehicleTrails(true)
--             SetForcePedFootstepsTracks(true)
            
--             if not loaded then
--                 RequestScriptAudioBank("ICE_FOOTSTEPS", false)
--                 RequestScriptAudioBank("SNOW_FOOTSTEPS", false)
--                 RequestNamedPtfxAsset("core_snow")
--                 while not HasNamedPtfxAssetLoaded("core_snow") do
--                     Citizen.Wait(0)
--                 end
--                 UseParticleFxAssetNextCall("core_snow")
--                 loaded = true
--             end
--             RequestAnimDict('anim@mp_snowball') -- pre-load the animation
--             if IsControlJustReleased(0, 119) and not IsPedInAnyVehicle(GetPlayerPed(-1), true) and not IsPlayerFreeAiming(PlayerId()) and not IsPedSwimming(PlayerPedId()) and not IsPedSwimmingUnderWater(PlayerPedId()) and not IsPedRagdoll(PlayerPedId()) and not IsPedFalling(PlayerPedId()) and not IsPedRunning(PlayerPedId()) and not IsPedSprinting(PlayerPedId()) and GetInteriorFromEntity(PlayerPedId()) == 0 and not IsPedShooting(PlayerPedId()) and not IsPedUsingAnyScenario(PlayerPedId()) and not IsPedInCover(PlayerPedId(), 0) then -- check if the snowball should be picked up
--                 TaskPlayAnim(PlayerPedId(), 'anim@mp_snowball', 'pickup_snowball', 8.0, -1, -1, 0, 1, 0, 0, 0) -- pickup the snowball
--                 Citizen.Wait(1950) -- wait 1.95 seconds to prevent spam clicking and getting a lot of snowballs without waiting for animatin to finish.
--                 GiveWeaponToPed(GetPlayerPed(-1), GetHashKey('WEAPON_SNOWBALL'), 2, false, true) -- get 2 snowballs each time.
--             end
--             if not IsPedInAnyVehicle(GetPlayerPed(-1), true) --[[and not IsPlayerFreeAiming(PlayerId())]] then
--                 if showHelp then
--                     showHelpNotification()
--                 end
--                 showHelp = false
--             else
--                 showHelp = true
--             end
--         else
--             -- disable frozen water effect
--             if loaded then N_0xc54a08c85ae4d410(0.0) end
--             loaded = false
--             RemoveNamedPtfxAsset("core_snow")
--             ReleaseNamedScriptAudioBank("ICE_FOOTSTEPS")
--             ReleaseNamedScriptAudioBank("SNOW_FOOTSTEPS")
--             SetForceVehicleTrails(false)
--             SetForcePedFootstepsTracks(false)
--         end
--         if GetSelectedPedWeapon(PlayerPedId()) == GetHashKey('WEAPON_SNOWBALL') then
--             SetPlayerWeaponDamageModifier(PlayerId(), 0.0)
--         end
--     end
-- end)

-- function showHelpNotification()
--     BeginTextCommandDisplayHelp("STRING")
--     AddTextComponentSubstringPlayerName("Press ~INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE~ while on foot, to pickup 2 snowballs!")
--     EndTextCommandDisplayHelp(0, 0, 1, -1)
-- end