resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

ui_page "ui/index.html"
files {
	"ui/index.html",
	"ui/assets/MadCity.png",
	"ui/imgs/test.png",
	"ui/fonts/fonts/Circular-Bold.ttf",
	"ui/fonts/fonts/Circular-Bold.ttf",
	"ui/fonts/fonts/Circular-Regular.ttf",
	"ui/script.js",
	"ui/style.css",
	"ui/debounce.min.js",
		-- Job Images
	'ui/assets/imgs/banker.png',
	-- 'ui/assets/imgs/unicorn.png',
	-- 'ui/assets/imgs/bus.png',
	-- 'ui/assets/imgs/cardealer.png',
	'ui/assets/imgs/detective.png',
	'ui/assets/imgs/ambulance.png',
	-- 'ui/assets/imgs/tailor.png',
	-- 'ui/assets/imgs/fisherman.png',
	-- 'ui/assets/imgs/garbage.png',
	'ui/assets/imgs/lumberjack.png',
	'ui/assets/imgs/slaughterer.png',
	'ui/assets/imgs/miner.png',
	-- 'ui/assets/imgs/pizza.png',
	'ui/assets/imgs/police.png',
	'ui/assets/imgs/gang.png',
	'ui/assets/imgs/mechanic.png',
	-- 'ui/assets/imgs/realestateagent.png',
	'ui/assets/imgs/banksecurity.png',
	'ui/assets/imgs/sheriff.png',
	'ui/assets/imgs/swat.png',
	-- 'ui/assets/imgs/fueler.png',
	'ui/assets/imgs/trucker.png',
	'ui/assets/imgs/taxi.png',
	'ui/assets/imgs/unemployed.png',
	-- 'ui/assets/imgs/postal.png',
	'ui/assets/imgs/offpolice.png',
	'ui/assets/imgs/offambulance.png',
	'ui/assets/imgs/offmechanic.png',
	'ui/assets/imgs/offtaxi.png',
	'ui/assets/imgs/psuspend.png'
}

client_scripts {
    'client.lua',
}

server_scripts {
    'server.lua',
}
