description 'Pola mare v1'

loadscreen_manual_shutdown 'yes'

files {
    'css/css.css',
    'imgs/bottomedge.svg',
    'imgs/forumbg.png',
    'imgs/headerbg2.jpg',
    'imgs/headerbg2.png',
    'imgs/headerbg2co.png',
    'imgs/headerbg3.jpg',
    'imgs/headerbg4.jpg',
    'imgs/headerbg5.jpg',
    'imgs/headerbg6.jpg',
    'imgs/highlight.svg',
    'imgs/highlightsmall.svg',
    'imgs/leftedge.svg',
    'imgs/logocover.svg',
    'imgs/logosvg.jpg',
    'imgs/logosvg.png',
    'imgs/logosvg.svg',
    'imgs/mixbg.png',
    'imgs/mixserverbg.svg',
    'imgs/playerbutton.svg',
    'imgs/rightedge.svg',
    'imgs/serverfivem.jpg',
    'imgs/serverfivem2.jpg',
    'imgs/serverfivem2jpg.jpg',
    'imgs/steamBG2.png',
    'imgs/teamspeakBG.png',
    'imgs/topedge.svg',
    'js/js.js',
    'index.html',
}

loadscreen 'index.html'
